package com.example.lotterykotlin

import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun check(view: View) {
        val text = findViewById<TextView>(R.id.number_edit).text
        if (text.length < 6) {
            Toast.makeText(this,
                this.getText(R.string.toast_error), Toast.LENGTH_LONG).show()
        } else {
            if (compareParts(text)) {
                findViewById<ImageView>(R.id.bulb_img)
                    .setColorFilter(this.getColor(R.color.green))
            } else {
                findViewById<ImageView>(R.id.bulb_img)
                    .setColorFilter(this.getColor(R.color.red))
            }
        }
    }

    private fun compareParts(text: CharSequence): Boolean {
       val first = text.subSequence(0, 3).sumOf { it.digitToInt() }
       val second = text.subSequence(3, 6).sumOf { it.digitToInt() }
       return first == second
    }
}